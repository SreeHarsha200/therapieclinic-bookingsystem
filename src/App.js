
import './App.css';
import Header from './components/Header';
import AvailableTimes from './components/AvailableTimes';
import Navigation from './components/Navigation';
import Filters from './components/Filters';
import { StyledContainer } from './components/styles/Container.styled';
import React, {useState, useEffect} from 'react'
import axios from 'axios'

const App = () =>{
  const [clinics,setClinics] = useState([])
  const [services, setServices] = useState([])

  useEffect(() => {
    const fetchClinics = async () => {
      const clinicsList = await axios(
          `https://siobddvpoa.execute-api.eu-west-1.amazonaws.com/harshas/clinics/`, {headers: {"x-api-key" : "tm2JPljkofr3MPMVaYk61iHxFCm3ahP82D1QJs18"}}
      )
      setClinics(clinicsList.data)
    }

    const fetchServices = async () => {
      const servicesList = await axios(
          `https://siobddvpoa.execute-api.eu-west-1.amazonaws.com/harshas/services/`, {headers: {"x-api-key" : "tm2JPljkofr3MPMVaYk61iHxFCm3ahP82D1QJs18"}}
      )
      setServices(servicesList.data)
    }

    fetchClinics()
    fetchServices()
  },[])

  return (
    <div>
        <Header />
        <StyledContainer>
          <Filters clinics={clinics} services={services}/>
          <AvailableTimes/>
        </StyledContainer>
    </div>
  );
}

export default App;
