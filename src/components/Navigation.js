import { StyledButton } from "./styles/Button.styled"
import { StyledTable, StyledTableRow } from "./styles/Navigation.styled"

const Navigation = () => {
    return (
        <div className='Navigation'>
            <StyledTable>
                <tr>
                    <StyledTableRow><StyledButton>Previous</StyledButton></StyledTableRow>
                    <StyledTableRow><StyledButton>Next</StyledButton></StyledTableRow>
                </tr>
            </StyledTable> 
      </div>
    )
}

export default Navigation
