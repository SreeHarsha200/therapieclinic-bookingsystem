import React from 'react'
import FilterTable from './FilterTable'
import { StyledFilters } from './styles/Filters.styled'
import { StyledFilterTableButton } from './styles/FilterTable.styled'

const Filters = ( {clinics, services}) => {
    return (
        <StyledFilters>
            <h1>Filters</h1>
            <FilterTable clinics={clinics} services={services}/>
            <StyledFilterTableButton>Search</StyledFilterTableButton>
        </StyledFilters>
    )
}

export default Filters
