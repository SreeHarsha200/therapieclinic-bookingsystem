
import { StyledButton } from './styles/Button.styled'

const Table = () => {
    return (
        <div className="table">
            <table>
                <tbody>
                    <tr>
                        <h1>Date</h1>
                        <td><StyledButton>9:00</StyledButton></td>
                        <td><StyledButton>10:00</StyledButton></td>
                        <td><StyledButton>11:00</StyledButton></td>
                        <td><StyledButton>12:00</StyledButton></td>
                        <td><StyledButton>13:00</StyledButton></td>
                        <td><StyledButton>14:00</StyledButton></td>
                        <td><StyledButton>15:00</StyledButton></td>
                        <td><StyledButton>16:00</StyledButton></td>
                    </tr>
                    <tr>
                        <h1>Date</h1>
                        <td><StyledButton>9:00</StyledButton></td>
                        <td><StyledButton>10:00</StyledButton></td>
                        <td><StyledButton>11:00</StyledButton></td>
                        <td><StyledButton>12:00</StyledButton></td>
                        <td><StyledButton>13:00</StyledButton></td>
                        <td><StyledButton>14:00</StyledButton></td>
                        <td><StyledButton>15:00</StyledButton></td>
                        <td><StyledButton>16:00</StyledButton></td>
                    </tr>
                    <tr>
                        <h1>Date</h1>
                        <td><StyledButton>9:00</StyledButton></td>
                        <td><StyledButton>10:00</StyledButton></td>
                        <td><StyledButton>11:00</StyledButton></td>
                        <td><StyledButton>12:00</StyledButton></td>
                        <td><StyledButton>13:00</StyledButton></td>
                        <td><StyledButton>14:00</StyledButton></td>
                        <td><StyledButton>15:00</StyledButton></td>
                        <td><StyledButton>16:00</StyledButton></td>
                    </tr>
                    <tr>
                        <h1>Date</h1>
                        <td><StyledButton>9:00</StyledButton></td>
                        <td><StyledButton>10:00</StyledButton></td>
                        <td><StyledButton>11:00</StyledButton></td>
                        <td><StyledButton>12:00</StyledButton></td>
                        <td><StyledButton>13:00</StyledButton></td>
                        <td><StyledButton>14:00</StyledButton></td>
                        <td><StyledButton>15:00</StyledButton></td>
                        <td><StyledButton>16:00</StyledButton></td>
                    </tr>
                    <tr>
                        <h1>Date</h1>
                        <td><StyledButton>9:00</StyledButton></td>
                        <td><StyledButton>10:00</StyledButton></td>
                        <td><StyledButton>11:00</StyledButton></td>
                        <td><StyledButton>12:00</StyledButton></td>
                        <td><StyledButton>13:00</StyledButton></td>
                        <td><StyledButton>14:00</StyledButton></td>
                        <td><StyledButton>15:00</StyledButton></td>
                        <td><StyledButton>16:00</StyledButton></td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default Table
