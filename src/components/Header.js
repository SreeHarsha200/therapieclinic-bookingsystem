
import {StyledHeader} from './styles/Header.styled'

const Header = () => {
    return (
        <StyledHeader>
            <h1>Therapie Availability Search</h1>
        </StyledHeader>
    )
}

export default Header
