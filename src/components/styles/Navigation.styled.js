import styled from "styled-components"

export const StyledTableRow = styled.td`
    height: 80px;
    width: 160px;
    text-align: center;
    vertical-align: middle;
`
export const StyledTable = styled.table`
    text-align: center;
    margin: 0 250px;
`