import styled from "styled-components"

export const StyledFilters = styled.div`
    text-align: center;
    font-size: 10px;
    padding-top: 50px;
    padding-left: 20px;
`