
  import styled from "styled-components"

  export const StyledFilterTable = styled.select`
  background-color: #c1aade;
  display: inline-block;
  color: white;
  padding: 10px 80px;
  margin: 10px;
  border-radius: 5px;
  cursor: pointer;
  text-decoration: none;
  font-size: 20px;
  font-family: inherit;
  border: none;
  cursor: pointer;
  width: 300px;
  `

  export const StyledFilterTableButton= styled.button`
  display: inline-block;
  background: #F2EAF4;
  color: black;
  border: none;
  padding: 10px 20px;
  margin: 5px;
  border-radius: 5px;
  cursor: pointer;
  text-decoration: none;
  font-size: 15px;
  font-family: inherit;
  font-weight: bold;
  `
