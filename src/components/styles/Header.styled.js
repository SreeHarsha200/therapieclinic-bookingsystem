import styled from "styled-components"

export const StyledHeader = styled.header`
    background-color: #c1aade;
    margin: 0 auto;
    padding: 1px ;
    color: white;
    max-width: 100%;
    text-align: center;
`