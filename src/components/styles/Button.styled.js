import styled from "styled-components"

export const StyledButton = styled.button`
display: inline-block;
background: #F2EAF4;
color: black;
border: none;
padding: 10px 20px;
margin: 5px;
border-radius: 5px;
cursor: pointer;
text-decoration: none;
font-size: 15px;
font-family: inherit;
font-weight: bold;
`