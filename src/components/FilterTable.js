import { StyledFilterTable } from "./styles/FilterTable.styled"
const FilterTable = ({clinics, services}) => {
    return (
        <div className='filterTable'>
            <table>
                <tr>
                    <StyledFilterTable name="clinic" id="Clinic">
                    <option value="clinic"> Select Clinic</option>
                        {clinics.map((clinic) => (
                            <option value={clinic.clinicId}> {clinic.streetAddress},{clinic.city}</option>
                        ))}
                    </StyledFilterTable>
                </tr>
                <tr>
                    <StyledFilterTable name="service" id="Service">
                    <option value="service"> Select Service</option>
                        {services.map((service) => (
                            <option value={service.serviceId}> {service.name}</option>
                        ))}
                    </StyledFilterTable>
                </tr> 
                <tr><StyledFilterTable name="cars" id="cars">
                    <option value="From Date">From Date</option>
                    <option value="saab">Saab</option>
                    <option value="mercedes">Mercedes</option>
                    <option value="audi">Audi</option>
                    </StyledFilterTable>
                </tr>
                <tr><StyledFilterTable name="cars" id="cars">
                    <option value="To Date">To Date</option>
                    <option value="saab">Saab</option>
                    <option value="mercedes">Mercedes</option>
                    <option value="audi">Audi</option>
                    </StyledFilterTable>
                </tr>
            </table>
        </div>
    )
}

export default FilterTable
