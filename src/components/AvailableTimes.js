
import {StyledAvailableTimes} from './styles/AvailableTimes.styled'
import Table from './Table'
import Navigation from './Navigation'

const AvailableTimes = () => {
    return (
        <StyledAvailableTimes>
            <h1>Available Times</h1>
            <Table/>
            <Navigation/>
        </StyledAvailableTimes>
    )
}

export default AvailableTimes
